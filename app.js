var express = require('express');
var cors = require('cors')
var app = express();
var bodyParser = require('body-parser')
var participantes = [];

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cors())

app.get('/participantes', function (req, res) {
    var filtrados = participantes;
    if (req.query.busca) {
      console.log(participantes);
        filtrados = participantes.filter(p =>
            p.matricula.toLocaleLowerCase().indexOf(req.query.busca.toLocaleLowerCase()) > -1
            || p.sigla.toLocaleLowerCase().indexOf(req.query.busca.toLocaleLowerCase()) > -1
          );
    }
  res.json(filtrados);
});

app.post('/participantes', function (req, res) {
    participantes.push({
      ...req.body,
      id: participantes.length == 0 ? 1 : (participantes.length + 1)
    });
  res.json(participantes);
});

app.get('/participantes/:id', function (req, res) {
  res.json(participantes.find(p => p.id == req.params.id));
});

app.put('/participantes/:id', function (req, res) {
    participantes = participantes.filter(p => p.id != req.params.id);
    participantes.push({
        ...req.body,
        id: req.params.id
    });
  res.json(participantes);
});

app.delete('/participantes/:id', function (req, res) {
    let index = participantes.findIndex(p => p.id == req.params.id);
    participantes.splice(index, 1);
  res.json(participantes);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});